package edu.washington.ramirs11.lifecounter;

/**
 * Manages Player status during game
 */

import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import java.util.ArrayList;

public class Player{

    private int lifeCount;  //total # of lives
    private String name;
    private boolean life;   //true = alive, false = dead

    public Player(){
        name = "newPlayer";
        lifeCount = 20;
        life = true;
    }
    public Player(int n, int n1){
        name = "player" + n;
        lifeCount = n1;
        if (n1 <= 0) {
            kill();
        } else {
            life = true;
        }
    }

    public int getLifeCount(){
        return lifeCount;
    }

    public String getName(){
        return name;
    }

    public void kill(){
        life = false;
    }

    public boolean deadYet(){
        return life;
    }

    public void minusLife(int n){
        lifeCount = lifeCount - n;
    }

    public void addLife(int n){
        lifeCount = lifeCount + n;
    }

    public void updateLifeCount(int n){lifeCount = lifeCount + n;}

    public String toString(){
        return name + ", lifeCount: " + lifeCount + ", alive: " + life;
    }

}
