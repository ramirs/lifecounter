package edu.washington.ramirs11.lifecounter;

import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    public static int lifeCount = 20;
    public static ArrayList<Player> players;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            initializePlayers(); //new game status
        } else {
            //old game status
            initializePlayers(savedInstanceState.getIntegerArrayList("lifeCounts"));
        }
        setContentView(R.layout.activity_main);

        //System.out.println("ArrayList size: " + players.size() + ", Players: " + players.toString()); debugging
        initializeGame();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putIntegerArrayList("lifeCounts", getPlayerCounts());
    }

    @Override
    public void onClick(View v){
        TextView lCount;
        switch (v.getId()) {
            //player 0 buttons
            case R.id.btn_minusMore_0:
                lCount = (TextView) findViewById(R.id.lifecount_0);
                lCount.setText("Lifecount: " + updateGame(0, -5));
                break;
            case R.id.btn_minus_0:
                lCount = (TextView) findViewById(R.id.lifecount_0);
                lCount.setText("Lifecount: " + updateGame(0, -1));
                break;
            case R.id.btn_plus_0:
                lCount = (TextView) findViewById(R.id.lifecount_0);
                lCount.setText("Lifecount: " + updateGame(0, 1));
                break;
            case R.id.btn_plusMore_0:
                lCount = (TextView) findViewById(R.id.lifecount_0);
                lCount.setText("Lifecount: " + updateGame(0, 5));
                break;
            //player 1 buttons
            case R.id.btn_minusMore_1:
                lCount = (TextView) findViewById(R.id.lifecount_1);
                lCount.setText("Lifecount: " + updateGame(1, -5));
                break;
            case R.id.btn_minus_1:
                lCount = (TextView) findViewById(R.id.lifecount_1);
                lCount.setText("Lifecount: " + updateGame(1, -1));
                break;
            case R.id.btn_plus_1:
                lCount = (TextView) findViewById(R.id.lifecount_1);
                lCount.setText("Lifecount: " + updateGame(1, 1));
                break;
            case R.id.btn_plusMore_1:
                lCount = (TextView) findViewById(R.id.lifecount_1);
                lCount.setText("Lifecount: " + updateGame(1, 5));
                break;
            //player 2 buttons
            case R.id.btn_minusMore_2:
                lCount = (TextView) findViewById(R.id.lifecount_2);
                lCount.setText("Lifecount: " + updateGame(2, -5));
                break;
            case R.id.btn_minus_2:
                lCount = (TextView) findViewById(R.id.lifecount_2);
                lCount.setText("Lifecount: " + updateGame(2, -1));
                break;
            case R.id.btn_plus_2:
                lCount = (TextView) findViewById(R.id.lifecount_2);
                lCount.setText("Lifecount: " + updateGame(2, 1));
                break;
            case R.id.btn_plusMore_2:
                lCount = (TextView) findViewById(R.id.lifecount_2);
                lCount.setText("Lifecount: " + updateGame(2, 5));
                break;
            //player 3 buttons
            case R.id.btn_minusMore_3:
                lCount = (TextView) findViewById(R.id.lifecount_3);
                lCount.setText("Lifecount: " + updateGame(3, -5));
                break;
            case R.id.btn_minus_3:
                lCount = (TextView) findViewById(R.id.lifecount_3);
                lCount.setText("Lifecount: " + updateGame(3, -1));
                break;
            case R.id.btn_plus_3:
                lCount = (TextView) findViewById(R.id.lifecount_3);
                lCount.setText("Lifecount: " + updateGame(3, 1));
                break;
            case R.id.btn_plusMore_3:
                lCount = (TextView) findViewById(R.id.lifecount_3);
                lCount.setText("Lifecount: " + updateGame(3, 5));
                break;
            default:
                break;
        }
        //System.out.println(players.toString()); debugging
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //starting a new game status
    public void initializePlayers(){
        players = new ArrayList<Player>();
        for (int i =0; i<4; i++){
            players.add(new Player(players.size(), 20));
        }
    }

    //re-initializing old game status
    public void initializePlayers(ArrayList<Integer> list){
        players = new ArrayList<Player>();
        for (Integer n : list){
            players.add(new Player(players.size(), n));
        }
    }

    public void initializeGame(){
        //player 0
        TextView lCount = (TextView) findViewById(R.id.lifecount_0);
        lCount.setText("Lifecount: " + players.get(0).getLifeCount());
        Button mM = (Button) findViewById(R.id.btn_minusMore_0);
        mM.setOnClickListener(this); // calling onClick() method
        Button m = (Button) findViewById(R.id.btn_minus_0);
        m.setOnClickListener(this); // calling onClick() method
        Button p = (Button) findViewById(R.id.btn_plus_0);
        p.setOnClickListener(this); // calling onClick() method
        Button pM = (Button) findViewById(R.id.btn_plusMore_0);
        pM.setOnClickListener(this); // calling onClick() method

        //player 1
        lCount = (TextView) findViewById(R.id.lifecount_1);
        lCount.setText("Lifecount: " + players.get(1).getLifeCount());
        mM = (Button) findViewById(R.id.btn_minusMore_1);
        mM.setOnClickListener(this); // calling onClick() method
        m = (Button) findViewById(R.id.btn_minus_1);
        m.setOnClickListener(this); // calling onClick() method
        p = (Button) findViewById(R.id.btn_plus_1);
        p.setOnClickListener(this); // calling onClick() method
        pM = (Button) findViewById(R.id.btn_plusMore_1);
        pM.setOnClickListener(this); // calling onClick() method

        //player 2
        lCount = (TextView) findViewById(R.id.lifecount_2);
        lCount.setText("Lifecount: " + players.get(2).getLifeCount());
        mM = (Button) findViewById(R.id.btn_minusMore_2);
        mM.setOnClickListener(this); // calling onClick() method
        m = (Button) findViewById(R.id.btn_minus_2);
        m.setOnClickListener(this); // calling onClick() method
        p = (Button) findViewById(R.id.btn_plus_2);
        p.setOnClickListener(this); // calling onClick() method
        pM = (Button) findViewById(R.id.btn_plusMore_2);
        pM.setOnClickListener(this); // calling onClick() method

        //player 3
        lCount = (TextView) findViewById(R.id.lifecount_3);
        lCount.setText("Lifecount: " + players.get(3).getLifeCount());
        mM = (Button) findViewById(R.id.btn_minusMore_3);
        mM.setOnClickListener(this); // calling onClick() method
        m = (Button) findViewById(R.id.btn_minus_3);
        m.setOnClickListener(this); // calling onClick() method
        p = (Button) findViewById(R.id.btn_plus_3);
        p.setOnClickListener(this); // calling onClick() method
        pM = (Button) findViewById(R.id.btn_plusMore_3);
        pM.setOnClickListener(this); // calling onClick() method
    }

    public int updateGame(int index, int n){
        Player player = players.get(index);
        player.updateLifeCount(n);
        if (player.getLifeCount() <= 0){
            //System.out.println("Number " + index + ".. dead!"); debugging
            player.kill();
            Toast.makeText(getApplicationContext(), "PLAYER " + index + " LOSES!!", Toast.LENGTH_SHORT).show();
        }
        players.set(index, player);
        return players.get(index).getLifeCount();
    }


    public ArrayList<Integer> getPlayerCounts(){
        ArrayList<Integer> lives = new ArrayList<Integer>();
        for(Player p : players){
            lives.add(p.getLifeCount());
        }
        return lives;
    }
}
